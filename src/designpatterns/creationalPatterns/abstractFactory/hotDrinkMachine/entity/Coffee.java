package designpatterns.creationalPatterns.abstractFactory.hotDrinkMachine.entity;

import designpatterns.creationalPatterns.abstractFactory.hotDrinkMachine.interfaces.IHotDrink;

public class Coffee implements IHotDrink {

	@Override
	public void consume() {
		System.out.println("This coffee is delicious.");		
	}

}
