package designpatterns.creationalPatterns.abstractFactory.hotDrinkMachine.entity.enums;

public enum AvailableDrink {
	
	COFFEE, TEA

}
