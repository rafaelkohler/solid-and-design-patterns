package designpatterns.creationalPatterns.abstractFactory.hotDrinkMachine.entity;

import designpatterns.creationalPatterns.abstractFactory.hotDrinkMachine.interfaces.IHotDrink;

public class Tea implements IHotDrink {

	@Override
	public void consume() {
		System.out.println("This tea is nice but I'd prefer it with milk.");
	}

}
