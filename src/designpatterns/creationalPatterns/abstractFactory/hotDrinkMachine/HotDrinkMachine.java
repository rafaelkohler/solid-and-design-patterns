package designpatterns.creationalPatterns.abstractFactory.hotDrinkMachine;

import java.util.HashMap;
import java.util.Map;

import designpatterns.creationalPatterns.abstractFactory.hotDrinkMachine.entity.enums.AvailableDrink;
import designpatterns.creationalPatterns.abstractFactory.hotDrinkMachine.interfaces.IHotDrink;
import designpatterns.creationalPatterns.abstractFactory.hotDrinkMachine.interfaces.IHotDrinkFactory;

public class HotDrinkMachine {

	private Map<AvailableDrink, IHotDrinkFactory> factories = new HashMap<>();

	public HotDrinkMachine() throws Exception {
		for (AvailableDrink drink : AvailableDrink.values()) {
			String s = drink.toString();
			String factoryName = "" + Character.toUpperCase(s.charAt(0)) + s.substring(1).toLowerCase();
			Class<?> factory = Class.forName("designpatterns.creationalPatterns.abstractFactory.hotDrinkMachine.factory." + factoryName + "Factory");
			factories.put(drink, (IHotDrinkFactory) factory.getDeclaredConstructor().newInstance());
		}
	}

	public IHotDrink makeDrink(AvailableDrink drink, int amount) {
		return ((IHotDrinkFactory) factories.get(drink)).prepare(amount);
	}
}
