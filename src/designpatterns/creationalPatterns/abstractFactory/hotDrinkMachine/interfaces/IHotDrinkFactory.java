package designpatterns.creationalPatterns.abstractFactory.hotDrinkMachine.interfaces;

public interface IHotDrinkFactory {

	IHotDrink prepare(int amount);
	
}
