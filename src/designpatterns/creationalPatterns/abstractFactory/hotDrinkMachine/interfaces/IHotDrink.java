package designpatterns.creationalPatterns.abstractFactory.hotDrinkMachine.interfaces;

public interface IHotDrink {
	
	void consume();

}
