package designpatterns.creationalPatterns.abstractFactory.hotDrinkMachine;

import designpatterns.creationalPatterns.abstractFactory.hotDrinkMachine.entity.enums.AvailableDrink;
import designpatterns.creationalPatterns.abstractFactory.hotDrinkMachine.interfaces.IHotDrink;

public class Application {
	
	public static void main(String[] args) throws Exception {
		 HotDrinkMachine machine = new HotDrinkMachine();
		    IHotDrink tea = machine.makeDrink(AvailableDrink.TEA, 200);
		    tea.consume();

		    // interactive
		    IHotDrink drink = machine.makeDrink(AvailableDrink.COFFEE, 50);
		    drink.consume();
	}

}
