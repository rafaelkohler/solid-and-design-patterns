package designpatterns.creationalPatterns.abstractFactory.hotDrinkMachine.factory;

import designpatterns.creationalPatterns.abstractFactory.hotDrinkMachine.entity.Coffee;
import designpatterns.creationalPatterns.abstractFactory.hotDrinkMachine.interfaces.IHotDrink;
import designpatterns.creationalPatterns.abstractFactory.hotDrinkMachine.interfaces.IHotDrinkFactory;

public class CoffeeFactory implements IHotDrinkFactory {

	@Override
	public IHotDrink prepare(int amount) {
		System.out.println("\nGrind some beans, boil water, pour " + amount + " ml, add cream and sugar, enjoy!");
		return new Coffee();
	}

}
