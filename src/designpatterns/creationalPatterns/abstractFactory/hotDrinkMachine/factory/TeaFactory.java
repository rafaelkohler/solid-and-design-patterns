package designpatterns.creationalPatterns.abstractFactory.hotDrinkMachine.factory;

import designpatterns.creationalPatterns.abstractFactory.hotDrinkMachine.entity.Tea;
import designpatterns.creationalPatterns.abstractFactory.hotDrinkMachine.interfaces.IHotDrink;
import designpatterns.creationalPatterns.abstractFactory.hotDrinkMachine.interfaces.IHotDrinkFactory;

public class TeaFactory implements IHotDrinkFactory {

	@Override
	public IHotDrink prepare(int amount) {
		System.out.println("\nPut in tea bag, boil water, pour " + amount + "ml, add lemon, enjoy!");
		return new Tea();
	}

}
