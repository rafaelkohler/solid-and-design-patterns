package designpatterns.creationalPatterns.builder.multipleBuilder;

import designpatterns.creationalPatterns.builder.multipleBuilder.builders.PersonBuilder;
import designpatterns.creationalPatterns.builder.multipleBuilder.entity.Person;

public class Application {

	public static void main(String[] args) {

		PersonBuilder pb = new PersonBuilder();
		Person person = pb
				.lives().at("123 London Road").in("London").withPostCode("SW12BC")
				.works().at("Fabrikan").asA("Engineer").earning(123000).build();
		
		System.out.println(person);

	}

}
