package designpatterns.creationalPatterns.builder.multipleBuilder.entity;

public class Person {
	
	//address
	public String streetAddress, postcode, city;
	
	//employment
	public String companyName, position;
	public int annualIncome;
	
	
	@Override
	public String toString() {
		return "Person [streetAAddress=" + streetAddress + ", postcode=" + postcode + ", city=" + city
				+ ", companyName=" + companyName + ", position=" + position + ", annualIncome=" + annualIncome + "]";
	}
	
	

}
