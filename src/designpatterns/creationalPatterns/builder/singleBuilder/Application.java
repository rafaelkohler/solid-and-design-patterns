package designpatterns.creationalPatterns.builder.singleBuilder;

import designpatterns.creationalPatterns.builder.singleBuilder.entity.EmployeeBuilder;
import designpatterns.creationalPatterns.builder.singleBuilder.entity.Person;

public class Application {

	public static void main(String[] args) {

		HtmlBuilder builder = new HtmlBuilder("ul");
		builder.addChild("li", "hello").addChild("li", "world");

		System.out.println(builder);
		
		EmployeeBuilder pb = new EmployeeBuilder();
		Person rafael = pb
				.withName("Rafael")
				.worksAt("Developer")
				.build();
		
		System.out.println(rafael);

	}

}
