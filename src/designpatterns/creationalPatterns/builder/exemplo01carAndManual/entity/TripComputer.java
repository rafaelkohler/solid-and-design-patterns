package designpatterns.creationalPatterns.builder.exemplo01carAndManual.entity;

public class TripComputer {
	
	private Car car;
	
	public void setCar(Car car) {
		this.car = car;
	}
	
	public void showFuelLevel() {
		System.out.println("Fuel level: " + car.getFuel()); 
	}
	
	public void showStatus() {
		
	}

}
