package designpatterns.creationalPatterns.builder.exemplo01carAndManual.entity;

public class GPSNavigator {
	
	private String route;
	
	public GPSNavigator() {
		this.route = "221b, Baker Street, London  to Scotland Yard, 8-10 Broadway, London";
	}

	public GPSNavigator(String manuelRoute) {
		super();
		this.route = manuelRoute;
	}

	public String getRoute() {
		return route;
	}
	
}
