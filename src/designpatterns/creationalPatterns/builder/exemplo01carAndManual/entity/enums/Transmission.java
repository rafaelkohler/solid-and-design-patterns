package designpatterns.creationalPatterns.builder.exemplo01carAndManual.entity.enums;

public enum Transmission {
	
	SINGLE_SPEED,
	MANUAL,
	AUTOMATIC,
	SEMI_AUTOMATIC

}
