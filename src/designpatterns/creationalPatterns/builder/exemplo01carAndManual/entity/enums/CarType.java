package designpatterns.creationalPatterns.builder.exemplo01carAndManual.entity.enums;

public enum CarType {
	
	CITY_CAR,
	SPORTS_CAR,
	SUV

}
