package designpatterns.creationalPatterns.builder.exemplo01carAndManual;

import designpatterns.creationalPatterns.builder.exemplo01carAndManual.builders.Builder;
import designpatterns.creationalPatterns.builder.exemplo01carAndManual.entity.Engine;
import designpatterns.creationalPatterns.builder.exemplo01carAndManual.entity.GPSNavigator;
import designpatterns.creationalPatterns.builder.exemplo01carAndManual.entity.TripComputer;
import designpatterns.creationalPatterns.builder.exemplo01carAndManual.entity.enums.CarType;
import designpatterns.creationalPatterns.builder.exemplo01carAndManual.entity.enums.Transmission;


/**
 * 
 * O Director define as etapas da ordem de constru��o do objeto.
 * Funciona com um objeto Builder concreto atrav�s de uma interface Builder.
 * Portanto ele n�o sabe qual o produto/objeto que est� sendo constru�do.
 *
 */
public class Director {
	
	public void constructSportsCar(Builder builder) {
		builder.setCarType(CarType.SPORTS_CAR);
		builder.setSeats(2);
		builder.setEngine(new Engine(3.0, 0));
		builder.setTransmission(Transmission.SEMI_AUTOMATIC);
		builder.setTripComputer(new TripComputer());
		builder.setGPSNavigator(new GPSNavigator());
	}
	
	public void constructCityCar(Builder builder) {
		builder.setCarType(CarType.CITY_CAR);
		builder.setSeats(2);
		builder.setEngine(new Engine(1.2, 0));
		builder.setTransmission(Transmission.AUTOMATIC);
		builder.setTripComputer(new TripComputer());
		builder.setGPSNavigator(new GPSNavigator());
	}
	
	public void constructSUV(Builder builder) {
		builder.setCarType(CarType.SUV);
		builder.setSeats(4);
		builder.setEngine(new Engine(2.5, 0));
		builder.setTransmission(Transmission.MANUAL);
		builder.setGPSNavigator(new GPSNavigator());
	}

}
