package designpatterns.creationalPatterns.builder.exemplo01carAndManual;

import designpatterns.creationalPatterns.builder.exemplo01carAndManual.builders.CarBuilder;
import designpatterns.creationalPatterns.builder.exemplo01carAndManual.builders.CarManualBuilder;
import designpatterns.creationalPatterns.builder.exemplo01carAndManual.entity.Car;
import designpatterns.creationalPatterns.builder.exemplo01carAndManual.entity.Manual;

public class Application {

	public static void main(String[] args) {

		Director director = new Director();
		
		CarBuilder builder = new CarBuilder();
		director.constructSportsCar(builder);
		
		Car car = builder.getResult();
		System.out.println("Car built:\n" + car.getCarType());
		
		CarManualBuilder manualBuilder = new CarManualBuilder();
		director.constructSportsCar(manualBuilder);
		
		Manual carManual = manualBuilder.getResult();
		System.out.println("\nCar manul built:\n" + carManual.print());
		
		
	}

}
