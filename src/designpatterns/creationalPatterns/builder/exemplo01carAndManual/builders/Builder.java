package designpatterns.creationalPatterns.builder.exemplo01carAndManual.builders;

import designpatterns.creationalPatterns.builder.exemplo01carAndManual.entity.Engine;
import designpatterns.creationalPatterns.builder.exemplo01carAndManual.entity.GPSNavigator;
import designpatterns.creationalPatterns.builder.exemplo01carAndManual.entity.TripComputer;
import designpatterns.creationalPatterns.builder.exemplo01carAndManual.entity.enums.CarType;
import designpatterns.creationalPatterns.builder.exemplo01carAndManual.entity.enums.Transmission;

public interface Builder {
	
	void setCarType(CarType type);
	void setSeats(int seats);
	void setEngine(Engine engine);
	void setTransmission(Transmission transmission);
	void setTripComputer(TripComputer tripComputer);
	void setGPSNavigator(GPSNavigator gpsNavigator);
	
}
