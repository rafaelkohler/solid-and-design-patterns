package designpatterns.creationalPatterns.builder.exemplo02bankAccount;

public class BankAccountBuilder {
	
	protected long accountNumber; //esta vari�vel vai ser passada atrav�s do construtor
	protected String owner;
	protected String branch;
	protected double balance;
	protected double interestRate;
	
	public BankAccountBuilder(long accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	public BankAccountBuilder withOwner(String owner) {
		this.owner = owner;
		
		return this;
	}
	
	public BankAccountBuilder atBranch(String branch) {
		this.branch = branch;
		
		return this;
	}
	
	public BankAccountBuilder openingBalance(double balance) {
		this.balance = balance;
		
		return this;
	}
	
	public BankAccountBuilder atRate(double interestRate) {
		this.interestRate = interestRate;
		
		return this;
	}
	
	public BankAccount build() {
		BankAccount account = new BankAccount(this.accountNumber);
		account.accountNumber = this.accountNumber;
		account.owner = this.owner;
		account.branch = this.branch;
		account.balance = this.balance;
		account.interestRate = this.interestRate;
		
		return account;
		
	}

}
