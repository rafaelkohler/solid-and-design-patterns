package designpatterns.creationalPatterns.builder.exemplo02bankAccount;

public class BankAccount {
	
	long accountNumber; //esta vari�vel vai ser passada atrav�s do construtor
	String owner;
	String branch;
	double balance;
	double interestRate;
	
	public BankAccount(long accountNumber) {
		this.accountNumber = accountNumber;
	}

	public long getAccountNumber() {
		return accountNumber;
	}

	public String getOwner() {
		return owner;
	}

	public String getBranch() {
		return branch;
	}

	public double getBalance() {
		return balance;
	}

	public double getInterestRate() {
		return interestRate;
	}

	@Override
	public String toString() {
		return "BankAccount [accountNumber=" + accountNumber + ", owner=" + owner + ", branch=" + branch + ", balance="
				+ balance + ", interestRate=" + interestRate + "]";
	}
	
	

}
