package designpatterns.creationalPatterns.builder.exemplo02bankAccount;

public class Application {

	public static void main(String[] args) {

		BankAccount account1 = new BankAccountBuilder(1234L)
				.withOwner("Marge")
				.atBranch("Springfield")
				.openingBalance(100)
				.atRate(2.5)
				.build();
		
		BankAccount account2 = new BankAccountBuilder(4567L)
				.withOwner("Homer")
				.atBranch("Springfield")
				.openingBalance(1000)
				.atRate(2.4)
				.build();
		
		System.out.println(account1.toString());
		
		System.out.println();
		System.out.println(account2);
				        
	}

}
