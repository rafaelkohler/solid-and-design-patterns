package designpatterns.creationalPatterns.builder.exercise;

public class CodeBuilder {
	
	private CodeElement root = new CodeElement();
	
	public CodeBuilder(String className) {
		root.className = className;
	}
	
	public CodeBuilder addField(String name, String type) {
		CodeElement codeElement = new CodeElement(name, type);
		root.elements.add(codeElement);
		return this;
	}
	
	@Override
	public String toString() {
		return root.toString();
		
	}

}
