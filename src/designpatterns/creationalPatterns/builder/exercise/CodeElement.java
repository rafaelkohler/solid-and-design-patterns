package designpatterns.creationalPatterns.builder.exercise;

import java.util.ArrayList;
import java.util.Collections;

public class CodeElement {

	public String className, name, type;
	public ArrayList<CodeElement> elements = new ArrayList<>();
	private final int indentSize = 2;
	private final String newLine = System.lineSeparator();

	public CodeElement() {
	}

	public CodeElement(String name, String type) {
		this.name = name;
		this.type = type;
	}

	private String toStringImpl(int indent) {
		StringBuilder sb = new StringBuilder();
		String i = String.join("", Collections.nCopies(indent * indentSize, " "));
		if (className != null && !className.isEmpty()) {
			sb.append(String.format("public%s class %s {%s%s", i, className, newLine, newLine));
		}
		if ((type != null && !type.isEmpty()) && (name != null && !name.isEmpty())) {
			sb.append(String.join("", Collections.nCopies(indentSize * (indent + 1), " ")))
					.append("public " + type + " ").append(name).append(newLine);
		}

		for (CodeElement e : elements) {
			sb.append(e.toStringImpl(indent+1));
		}

		return sb.toString();
	}

	@Override
	public String toString() {
		return toStringImpl(0);
	}

}
