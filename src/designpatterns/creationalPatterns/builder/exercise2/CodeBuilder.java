package designpatterns.creationalPatterns.builder.exercise2;

public class CodeBuilder {

	private Code theClass = new Code();

	public CodeBuilder(String rootName) {
		theClass.name = rootName;
	}

	public CodeBuilder addField(String name, String type) {
		theClass.fields.add(new Field(name, type));
		return this;
	}

	@Override
	public String toString() {
		return theClass.toString();
	}

}
