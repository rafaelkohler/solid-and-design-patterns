package designpatterns.creationalPatterns.builder.exercise2;

import java.util.ArrayList;
import java.util.List;

public class Code {

	public String name;
	public List<Field> fields = new ArrayList<>();

	public Code() {
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		String nl = System.lineSeparator();
		sb.append("public class " + name).append(" {").append(nl).append(nl);
		for (Field f : fields)
			sb.append("  " + f).append(nl);
		sb.append(nl).append("}").append(nl);
		return sb.toString();
	}

}
