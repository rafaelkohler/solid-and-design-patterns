package designpatterns.creationalPatterns.factoryMethod.formas.interfaces;

public interface Shape {
	
	void draw();

}
