package designpatterns.creationalPatterns.factoryMethod.formas.classesConcretas;

import designpatterns.creationalPatterns.factoryMethod.formas.interfaces.Shape;

public class Square implements Shape {

	@Override
	public void draw() {
		System.out.println("Inside Square::draw() method.");
	}

}
