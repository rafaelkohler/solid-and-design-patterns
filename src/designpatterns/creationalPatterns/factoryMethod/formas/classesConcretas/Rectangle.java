package designpatterns.creationalPatterns.factoryMethod.formas.classesConcretas;

import designpatterns.creationalPatterns.factoryMethod.formas.interfaces.Shape;

public class Rectangle implements Shape{

	@Override
	public void draw() {
		System.out.println("Inside Rectangle::draw() method.");
	}

}
