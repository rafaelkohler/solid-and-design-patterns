package designpatterns.creationalPatterns.factoryMethod.formas.classesConcretas;

import designpatterns.creationalPatterns.factoryMethod.formas.interfaces.Shape;

public class Circle implements Shape {

	@Override
	public void draw() {
		System.out.println("Inside Circle::draw() method.");
	}

}
