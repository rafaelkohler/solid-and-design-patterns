package designpatterns.creationalPatterns.factoryMethod.formas;

import designpatterns.creationalPatterns.factoryMethod.formas.factory.ShapeFactory;
import designpatterns.creationalPatterns.factoryMethod.formas.interfaces.Shape;

public class Application {

	public static void main(String[] args) {

		ShapeFactory factory = new ShapeFactory();

		Shape shape1 = factory.getCircle();
		shape1.draw();

		Shape shape2 = factory.getShape("RECTANGLE");
		shape2.draw();

		Shape shape3 = factory.getSquare();
		shape3.draw();

	}

}
