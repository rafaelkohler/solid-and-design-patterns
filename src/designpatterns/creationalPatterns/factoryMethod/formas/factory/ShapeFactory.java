package designpatterns.creationalPatterns.factoryMethod.formas.factory;

import designpatterns.creationalPatterns.factoryMethod.formas.classesConcretas.Circle;
import designpatterns.creationalPatterns.factoryMethod.formas.classesConcretas.Rectangle;
import designpatterns.creationalPatterns.factoryMethod.formas.classesConcretas.Square;
import designpatterns.creationalPatterns.factoryMethod.formas.interfaces.Shape;

/**
 * 
 * @author Rafael Kohler Gera objetos de classe concreta com base em informações
 *         fornecidas.
 *
 */
public class ShapeFactory {

	public Shape getRectangle() {
		return new Rectangle();
	}

	public Shape getCircle() {
		return new Circle();
	}

	public Shape getSquare() {
		return new Square();
	}

	public Shape getShape(String shapeType) {
		if (shapeType == null) {
			return null;
		}
		if (shapeType.equalsIgnoreCase("CIRCLE")) {
			return new Circle();

		} else if (shapeType.equalsIgnoreCase("RECTANGLE")) {
			return new Rectangle();

		} else if (shapeType.equalsIgnoreCase("SQUARE")) {
			return new Square();
		}

		return null;
	}

}
