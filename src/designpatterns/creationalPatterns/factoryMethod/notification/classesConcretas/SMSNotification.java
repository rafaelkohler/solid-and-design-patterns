package designpatterns.creationalPatterns.factoryMethod.notification.classesConcretas;

import designpatterns.creationalPatterns.factoryMethod.notification.interfaces.Notification;

public class SMSNotification implements Notification {

	@Override
	public void notifyUser() {
		System.out.println("Sending an SMS notification");
	}

}
