package designpatterns.creationalPatterns.factoryMethod.notification.classesConcretas;

import designpatterns.creationalPatterns.factoryMethod.notification.interfaces.Notification;

public class PushNotification implements Notification {

	@Override
	public void notifyUser() {
		System.out.println("Sending a push notification");
	}

}
