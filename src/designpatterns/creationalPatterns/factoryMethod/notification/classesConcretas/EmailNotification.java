package designpatterns.creationalPatterns.factoryMethod.notification.classesConcretas;

import designpatterns.creationalPatterns.factoryMethod.notification.interfaces.Notification;

public class EmailNotification implements Notification {

	@Override
	public void notifyUser() {
		System.out.println("Sending an e-mail notification");
	}

}
