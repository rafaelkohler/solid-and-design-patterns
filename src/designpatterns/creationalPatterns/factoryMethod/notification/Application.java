package designpatterns.creationalPatterns.factoryMethod.notification;

import designpatterns.creationalPatterns.factoryMethod.notification.factory.NotificationFactory;
import designpatterns.creationalPatterns.factoryMethod.notification.interfaces.Notification;

public class Application {
	
	public static void main(String[] args) {
		NotificationFactory notificationFactory = new NotificationFactory();
        Notification notification = notificationFactory.createNotification("SMS");
        notification.notifyUser();
        
        Notification push = notificationFactory.getPushNotification();
        push.notifyUser();
        
	}

}
