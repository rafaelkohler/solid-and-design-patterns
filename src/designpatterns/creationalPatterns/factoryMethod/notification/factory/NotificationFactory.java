package designpatterns.creationalPatterns.factoryMethod.notification.factory;

import designpatterns.creationalPatterns.factoryMethod.notification.classesConcretas.EmailNotification;
import designpatterns.creationalPatterns.factoryMethod.notification.classesConcretas.PushNotification;
import designpatterns.creationalPatterns.factoryMethod.notification.classesConcretas.SMSNotification;
import designpatterns.creationalPatterns.factoryMethod.notification.interfaces.Notification;

public class NotificationFactory {

	public Notification createNotification(String channel) {
		if (channel == null || channel.isEmpty())
			return null;
		if ("SMS".equals(channel)) {
			return new SMSNotification();
		} else if ("EMAIL".equals(channel)) {
			return new EmailNotification();
		} else if ("PUSH".equals(channel)) {
			return new PushNotification();
		}
		return null;
	}
	
	public Notification getSMSNotification() {
		return new SMSNotification();
	}
	
	public Notification getEmailNotification() {
		return new EmailNotification();
	}
	
	public Notification getPushNotification() {
		return new PushNotification();
	}

}
