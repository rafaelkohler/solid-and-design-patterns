package designpatterns.creationalPatterns.factoryMethod.notification.interfaces;

public interface Notification {
	
	void notifyUser();

}
