package designpatterns.creationalPatterns.factoryMethod.GUI.interfaces;

public interface Button {
	
	void render();
	void onClick();

}
