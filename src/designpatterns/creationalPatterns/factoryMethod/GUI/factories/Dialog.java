package designpatterns.creationalPatterns.factoryMethod.GUI.factories;

import designpatterns.creationalPatterns.factoryMethod.GUI.interfaces.Button;

public abstract class Dialog {
	
	public void renderWindow() {
		Button okButton = createButton();
        okButton.render();
	}
	
	public abstract Button createButton();

}
