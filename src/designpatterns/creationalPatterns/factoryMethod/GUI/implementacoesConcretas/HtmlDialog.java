package designpatterns.creationalPatterns.factoryMethod.GUI.implementacoesConcretas;

import designpatterns.creationalPatterns.factoryMethod.GUI.factories.Dialog;
import designpatterns.creationalPatterns.factoryMethod.GUI.interfaces.Button;

public class HtmlDialog extends Dialog {

	@Override
	public Button createButton() {
		return new HtmlButton();
	}

}
