package designpatterns.creationalPatterns.factoryMethod.GUI.implementacoesConcretas;

import designpatterns.creationalPatterns.factoryMethod.GUI.interfaces.Button;

public class HtmlButton implements Button {

	@Override
	public void render() {
		System.out.println("<button>Bot�o Teste</button>");
		onClick();
	}

	@Override
	public void onClick() {
		System.out.println("Click! - 'Bot�o clicado!'");
	}

}
