package designpatterns.creationalPatterns.factoryMethod.GUI.implementacoesConcretas;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import designpatterns.creationalPatterns.factoryMethod.GUI.interfaces.Button;

public class WindowsButton implements Button {

	JPanel panel = new JPanel();
	JFrame frame = new JFrame();
	JButton button;

	@Override
	public void render() {
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JLabel label = new JLabel("    Sistema Windows10.    ");
		label.setOpaque(true);
		label.setBackground(new Color(186, 22, 63));
		label.setFont(new Font("Dialog", Font.BOLD, 28));
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setHorizontalTextPosition(SwingConstants.CENTER);
		label.setSize(390, 40);
		panel.setLayout(new FlowLayout(FlowLayout.CENTER));
		frame.getContentPane().add(panel);
		panel.add(label);
		onClick();
		panel.add(button);

		frame.setSize(400, 200);
		frame.setVisible(true);
		onClick();
	}

	@Override
	public void onClick() {
		button = new JButton("Exit");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
				System.exit(0);
			}
		});
	}

}
