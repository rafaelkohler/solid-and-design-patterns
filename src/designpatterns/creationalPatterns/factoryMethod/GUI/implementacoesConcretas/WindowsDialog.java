package designpatterns.creationalPatterns.factoryMethod.GUI.implementacoesConcretas;

import designpatterns.creationalPatterns.factoryMethod.GUI.factories.Dialog;
import designpatterns.creationalPatterns.factoryMethod.GUI.interfaces.Button;

public class WindowsDialog extends Dialog {

	public Button createButton() {
		return new WindowsButton();
	}

}
