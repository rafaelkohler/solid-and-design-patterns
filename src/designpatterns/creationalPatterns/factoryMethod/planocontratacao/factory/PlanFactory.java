package designpatterns.creationalPatterns.factoryMethod.planocontratacao.factory;

import designpatterns.creationalPatterns.factoryMethod.planocontratacao.classesAbstratasEConcretas.CommercialPlan;
import designpatterns.creationalPatterns.factoryMethod.planocontratacao.classesAbstratasEConcretas.DomesticPlan;
import designpatterns.creationalPatterns.factoryMethod.planocontratacao.classesAbstratasEConcretas.InstitutionalPlan;
import designpatterns.creationalPatterns.factoryMethod.planocontratacao.classesAbstratasEConcretas.Plan;

public class PlanFactory {

	public Plan getPlan(String planType) {
		if (planType == null) {
			return null;
		}
		if (planType.equalsIgnoreCase("DOMESTICPLAN")) {
			return new DomesticPlan();
		} else if (planType.equalsIgnoreCase("COMMERCIALPLAN")) {
			return new CommercialPlan();
		} else if (planType.equalsIgnoreCase("INSTITUTIONALPLAN")) {
			return new InstitutionalPlan();
		}
		return null;
	}

}
