package designpatterns.creationalPatterns.factoryMethod.planocontratacao.classesAbstratasEConcretas;

public class InstitutionalPlan extends Plan {

	@Override
	public void getRate() {
		rate = 5.50;
	}

}
