package designpatterns.creationalPatterns.factoryMethod.coordenadas.entity;

public class Point {
	
	private double x, y;
	
	public Point(double a, double b) {
		this.x = a;
		this.y = b;
	}
	
	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	@Override
	public String toString() {
		return "Point [x=" + x + ", y=" + y + "]";
	}
	
}
