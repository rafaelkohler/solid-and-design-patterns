package designpatterns.creationalPatterns.factoryMethod.coordenadas;

import designpatterns.creationalPatterns.factoryMethod.coordenadas.entity.Point;
import designpatterns.creationalPatterns.factoryMethod.coordenadas.factory.PointFactory;

public class Application {

	public static void main(String[] args) {
		Point point = PointFactory.newPolarPoint(2,  3);

		System.out.println(point);
	}

}
