package designpatterns.SOLID.openClosedPrinciple.entity;

import designpatterns.SOLID.openClosedPrinciple.enums.Color;
import designpatterns.SOLID.openClosedPrinciple.enums.Size;

public class Product {
	
	public String name;
	public Color color;
	public Size size;
	
	public Product(String name, Color color, Size size) {
		this.name = name;
		this.color = color;
		this.size = size;
	}

}
