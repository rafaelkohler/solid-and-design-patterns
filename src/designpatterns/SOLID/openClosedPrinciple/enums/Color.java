package designpatterns.SOLID.openClosedPrinciple.enums;

public enum Color {
	RED, GREEN, BLUE
}
