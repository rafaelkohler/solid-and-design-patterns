package designpatterns.SOLID.openClosedPrinciple;

import java.util.Arrays;
import java.util.List;

import designpatterns.SOLID.openClosedPrinciple.entity.Product;
import designpatterns.SOLID.openClosedPrinciple.enums.Color;
import designpatterns.SOLID.openClosedPrinciple.enums.Size;
import designpatterns.SOLID.openClosedPrinciple.specifications.AndSpecification;
import designpatterns.SOLID.openClosedPrinciple.specifications.ColorSpecification;
import designpatterns.SOLID.openClosedPrinciple.specifications.SizeSpecification;

public class Application {

	public static void main(String... args) {

		Product apple = new Product("Apple", Color.GREEN, Size.SMALL);
		Product tree = new Product("Tree", Color.GREEN, Size.LARGE);
		Product house = new Product("House", Color.BLUE, Size.LARGE);

		List<Product> products = Arrays.asList(apple, tree, house);

		ProductFilter pf = new ProductFilter();
		System.out.println("Green products");
		pf.filter(products, new ColorSpecification(Color.GREEN))
				.forEach(p -> System.out.println(" - " + p.name + " is green."));
		
		System.out.println("\nLage products");
		pf.filter(products, new SizeSpecification(Size.LARGE))
				.forEach(p -> System.out.println(" - " + p.name + " is large."));
		
		System.out.println("\nLarge and blue items");
		pf.filter(products, new AndSpecification<>(
				new ColorSpecification(Color.BLUE), 
				new SizeSpecification(Size.LARGE)))
		.forEach(p -> System.out.println(" - " + p.name + " is large and blue."));

	}

}
