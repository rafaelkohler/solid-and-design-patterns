package designpatterns.SOLID.openClosedPrinciple.interfaces;

public abstract interface Specification<T> {
	
	boolean isStisfied(T item);
	
}
