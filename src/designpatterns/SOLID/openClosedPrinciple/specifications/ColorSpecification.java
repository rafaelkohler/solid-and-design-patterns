package designpatterns.SOLID.openClosedPrinciple.specifications;

import designpatterns.SOLID.openClosedPrinciple.entity.Product;
import designpatterns.SOLID.openClosedPrinciple.enums.Color;
import designpatterns.SOLID.openClosedPrinciple.interfaces.Specification;

public class ColorSpecification implements Specification<Product> {
	
	private Color color;

	public ColorSpecification(Color color) {
		this.color = color;
	}

	@Override
	public boolean isStisfied(Product item) {
		return item.color == color;
	}

}
