package designpatterns.SOLID.openClosedPrinciple.specifications;

import designpatterns.SOLID.openClosedPrinciple.interfaces.Specification;

public class AndSpecification<T> implements Specification<T> {

	private Specification<T> first, second;
	
	public AndSpecification(Specification<T> first, Specification<T> second) {
		this.first = first;
		this.second = second;
	}

	@Override
	public boolean isStisfied(T item) {
		return first.isStisfied(item) && second.isStisfied(item);
	}
	
}
