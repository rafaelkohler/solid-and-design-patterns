package designpatterns.SOLID.openClosedPrinciple.specifications;

import designpatterns.SOLID.openClosedPrinciple.entity.Product;
import designpatterns.SOLID.openClosedPrinciple.enums.Size;
import designpatterns.SOLID.openClosedPrinciple.interfaces.Specification;

public class SizeSpecification implements Specification<Product> {
	
	private Size size;

	public SizeSpecification(Size size) {
		this.size = size;
	}

	@Override
	public boolean isStisfied(Product item) {
		return item.size == size;
	}

}
