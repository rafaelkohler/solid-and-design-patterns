package designpatterns.SOLID.openClosedPrinciple;

import java.util.List;
import java.util.stream.Stream;

import designpatterns.SOLID.openClosedPrinciple.entity.Product;
import designpatterns.SOLID.openClosedPrinciple.interfaces.Filter;
import designpatterns.SOLID.openClosedPrinciple.interfaces.Specification;

public class ProductFilter implements Filter<Product> {
	
	@Override
	public Stream<Product> filter(List<Product> items, Specification<Product> spec) {
		return items.stream().filter(p -> spec.isStisfied(p));
	}
	
}
