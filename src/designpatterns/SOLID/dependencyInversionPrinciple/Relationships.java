package designpatterns.SOLID.dependencyInversionPrinciple;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.javatuples.Triplet;

import designpatterns.SOLID.dependencyInversionPrinciple.entity.Person;
import designpatterns.SOLID.dependencyInversionPrinciple.enums.Relationship;
import designpatterns.SOLID.dependencyInversionPrinciple.interfaces.RelationshipBrowser;

public class Relationships implements RelationshipBrowser {

	private List<Triplet<Person, Relationship, Person>> relations = new ArrayList<>();

	public List<Triplet<Person, Relationship, Person>> getRelations() {
		return relations;
	}

	public void addParentAndChild(Person parent, Person child) {
		relations.add(new Triplet<>(parent, Relationship.PARENT, child));
		relations.add(new Triplet<>(child, Relationship.CHILD, parent));
	}

	@Override
	public List<Person> findAllChildrenOff(String name) {
		return relations.stream()
				.filter(x -> Objects.equals(x.getValue0().name, name) && x.getValue1() == Relationship.PARENT)
				.map(Triplet::getValue2).collect(Collectors.toList());
	}

}
