package designpatterns.SOLID.dependencyInversionPrinciple.enums;

public enum Relationship {
	
	  PARENT,
	  CHILD,
	  SIBLING

}
