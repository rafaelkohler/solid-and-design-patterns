package designpatterns.SOLID.dependencyInversionPrinciple.interfaces;

import java.util.List;

import designpatterns.SOLID.dependencyInversionPrinciple.entity.Person;

public interface RelationshipBrowser {
	
	List<Person> findAllChildrenOff(String name);

}
