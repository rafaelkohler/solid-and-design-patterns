package designpatterns.SOLID.singleResponsabilityPrinciple;

public class Application {

	public static void main(String[] args) throws Exception {
		Journal j = new Journal();
		j.addEntry("I cried today");
		j.addEntry("I ate a bug");
		j.addEntry("Test");
		
		Persistence p = new Persistence();
		String filename = "C:\\temp\\journal.txt";
		p.saveToFile(j, filename, true);
		
		Runtime.getRuntime().exec("notepad.exe " + filename);
	}

}
