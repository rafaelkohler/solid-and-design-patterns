package designpatterns.SOLID.liskovSubstitutionPrincipel;

public class Application {
	
	public static void useIt(Rectangle r) {
		int width = r.getWidth();
		r.setHeight(10);
		
		System.out.println("Expected area of " + (width * 10) +
				", got " + r.getArea());
	}

	public static void main(String[] args) {
		
		Rectangle rc = RectangleFactory.newRectangle(2, 3);
		useIt(rc);
		
		Rectangle sq = RectangleFactory.newSquare(5);
		useIt(sq);
	}

}
