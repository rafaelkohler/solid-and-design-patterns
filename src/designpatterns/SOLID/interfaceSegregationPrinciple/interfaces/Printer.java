package designpatterns.SOLID.interfaceSegregationPrinciple.interfaces;

import designpatterns.SOLID.interfaceSegregationPrinciple.entity.Document;

public interface Printer {
	
	public void print(Document d);

}
