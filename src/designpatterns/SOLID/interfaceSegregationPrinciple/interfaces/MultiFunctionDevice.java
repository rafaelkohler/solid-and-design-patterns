package designpatterns.SOLID.interfaceSegregationPrinciple.interfaces;

public interface MultiFunctionDevice extends Printer, Scanner {
	
}
