package designpatterns.SOLID.interfaceSegregationPrinciple.interfaces;

import designpatterns.SOLID.interfaceSegregationPrinciple.entity.Document;

public interface Scanner {
	
	public void scan(Document d);

}
