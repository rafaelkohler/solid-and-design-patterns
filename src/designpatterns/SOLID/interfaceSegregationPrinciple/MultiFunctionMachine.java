package designpatterns.SOLID.interfaceSegregationPrinciple;

import designpatterns.SOLID.interfaceSegregationPrinciple.entity.Document;
import designpatterns.SOLID.interfaceSegregationPrinciple.interfaces.MultiFunctionDevice;
import designpatterns.SOLID.interfaceSegregationPrinciple.interfaces.Printer;
import designpatterns.SOLID.interfaceSegregationPrinciple.interfaces.Scanner;

public class MultiFunctionMachine implements MultiFunctionDevice {

	private Printer printer;
	private Scanner scanner;
	
	public MultiFunctionMachine(Printer printer, Scanner scanner) {
		this.printer = printer;
		this.scanner = scanner;
	}
	
	@Override
	public void print(Document d) {
		printer.print(d);
	}

	@Override
	public void scan(Document d) {
		scanner.scan(d);
	}
	
}
