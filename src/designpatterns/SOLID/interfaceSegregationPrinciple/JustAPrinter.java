package designpatterns.SOLID.interfaceSegregationPrinciple;

import designpatterns.SOLID.interfaceSegregationPrinciple.entity.Document;
import designpatterns.SOLID.interfaceSegregationPrinciple.interfaces.Printer;

public class JustAPrinter implements Printer {

	@Override
	public void print(Document d) {
		
	}

}
