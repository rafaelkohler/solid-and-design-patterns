package designpatterns.SOLID.interfaceSegregationPrinciple;

import designpatterns.SOLID.interfaceSegregationPrinciple.entity.Document;
import designpatterns.SOLID.interfaceSegregationPrinciple.interfaces.Printer;
import designpatterns.SOLID.interfaceSegregationPrinciple.interfaces.Scanner;

public class Photocopier implements Printer, Scanner {

	@Override
	public void print(Document d) {
		
	}

	@Override
	public void scan(Document d) {
		
	}

}
